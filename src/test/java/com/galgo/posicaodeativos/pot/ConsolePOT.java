package com.galgo.posicaodeativos.pot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by valdemar.arantes on 06/05/2016.
 */
public class ConsolePOT {

    private static final Logger log = LoggerFactory.getLogger(ConsolePOT.class);

    public static void main(String... args) {
        String acao = "ação";
        System.out.println(acao);
        log.debug(acao);
    }
}

