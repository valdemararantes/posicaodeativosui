package com.galgo.posicaodeativosui;

import com.galgo.utils.ApplicationException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ConfigFolderPreferences {

    private static final Logger log = LoggerFactory.getLogger(ConfigFolderPreferences.class);
    public final static String EXT_CONFIG_PATH_KEY = "config-file.path";

    @FXML private GridPane bodyPane;

    @FXML private Label lblCurrentPath;

    @FXML private TextField txtCurrentPath;

    @FXML private Label lblNewPath;

    @FXML private TextField txtNewPath;

    @FXML private Button btnConfirmar;

    @FXML private Button btnCancelar;

    private void confirmar() {
        try {
            String newPathStr = txtNewPath.getText();
            if (StringUtils.isNotBlank(txtNewPath.getText())) {
                Path newPath = Paths.get(newPathStr);
                File newConfFile = newPath.toFile();
                if (newConfFile.isFile()) {
                    log.info("Nova pasta do arquivo de configuração:\n{}", newPath);
                    saveFormValues();
                    Config.getInstance().reload(newConfFile);
                    ((Stage) bodyPane.getScene().getWindow()).close();
                } else if (newConfFile.isDirectory()) {
                    Alert.show("O path " + newPath + " não pode ser selecionado porque já existe uma pasta com este nome.",
                        javafx.scene.control.Alert.AlertType.ERROR);
                    return;
                } else {
                    if (!newConfFile.getParentFile().isDirectory()) {
                        if (!newConfFile.getParentFile().mkdirs()) {
                            Alert.show("Não foi possível criar a pasta " + newConfFile.getParentFile().getAbsolutePath(),
                                javafx.scene.control.Alert.AlertType.ERROR);
                            return;
                        }
                    }
                    if (!newConfFile.createNewFile()) {
                        Alert.show("Erro ao criar o arquivo " + newConfFile.getAbsolutePath(), javafx.scene.control.Alert.AlertType.ERROR);
                        return;
                    }
                    saveFormValues();
                    Config.getInstance().reload(newConfFile);
                    ((Stage) bodyPane.getScene().getWindow()).close();
                }
            } else {
                Alert.show("O caminho da nova pasta é obrigatório", javafx.scene.control.Alert.AlertType.ERROR);
            }
        } catch (ApplicationException e) {
            log.error(null, e);
            Alert.show(e.getMessage(), javafx.scene.control.Alert.AlertType.ERROR);
        } catch (Exception e) {
            String errMsg = "Erro ao alterar a pasta do arquivo de configurações";
            log.error(errMsg, e);
            Alert.show(errMsg, javafx.scene.control.Alert.AlertType.ERROR);
        }
    }

    @FXML
    void handleBtnCancelarAction(ActionEvent event) {
        log.info("Click no botão Cancelar. Fechando a janela de preferências...");
        ((Stage) bodyPane.getScene().getWindow()).close();
    }

    @FXML
    void handleBtnCancelarKeyPressed(KeyEvent event) {
        if (KeyCode.ENTER.equals(event.getCode())) {
            log.info("ENTER no botão Cancelar");
            ((Stage) bodyPane.getScene().getWindow()).close();
        }
    }

    @FXML
    void handleBtnSelectFolderAction(ActionEvent event) {
        log.info("Click no botão ... (Selecionar arquivo)");
        selectFolder();
    }

    @FXML
    void handleBtnBtnSelectFolderKeyPressed(KeyEvent event) {
        if (KeyCode.ENTER.equals(event.getCode())) {
            log.info("ENTER no botão ... (Selecionar arquivo)");
            selectFolder();
        }
    }

    @FXML
    void handleBtnConfirmarAction(ActionEvent event) {
        log.info("Click no botão Confirmar");
        confirmar();
    }

    @FXML
    void handleBtnConfirmarKeyPressed(KeyEvent event) {
        if (KeyCode.ENTER.equals(event.getCode())) {
            log.info("ENTER no botão Confirmar");
            confirmar();
        }
    }

    @FXML
    void initialize() {
        loadFormValues();
    }

    private void selectFolder() {
        FileChooser chooser = new FileChooser();
        chooser.setTitle(AppConfig.getInstance().getProperty("msg.title.select_file"));
        File defaultDirectory = new File(txtCurrentPath.getText()).getParentFile();
        chooser.setInitialDirectory(defaultDirectory);
        File selectedDirectory = chooser.showSaveDialog(MainUI.getInstance().primaryStage);
        if (selectedDirectory != null) {
            txtNewPath.setText(selectedDirectory.getAbsolutePath());
            log.info(selectedDirectory.toString());
        } else {
            log.info("Nenhum arquivo foi selecionado");
        }
    }

    private void loadFormValues() {
        String extConfiPathProp = MainUI.externalConfig.getProperty(EXT_CONFIG_PATH_KEY);
        if (StringUtils.isBlank(extConfiPathProp)) {
            try {
                String extConfigPath = Config.getInstance().getConfigFile().getCanonicalPath();
                txtCurrentPath.setText(extConfigPath);
            } catch (IOException e) {
                throw new ApplicationException(e);
            }
        } else {
            txtCurrentPath.setText(extConfiPathProp);
        }
    }

    private void saveFormValues() {
        log.info("Salvando valores do formulário");
        MainUI.externalConfig.setProperty(EXT_CONFIG_PATH_KEY, txtNewPath.getText());
        MainUI.externalConfig.save();
    }
}
