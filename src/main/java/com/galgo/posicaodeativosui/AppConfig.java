package com.galgo.posicaodeativosui;

import com.galgo.utils.ApplicationException;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Properties;

/**
 * Carrega as configuracoes do aplicativo
 *
 * @author valdemar.arantes
 */
public class AppConfig {

    public static final String CONNECTION_RECEIVE_TIMEOUT = "connection.receive.timeout";
    private static final Logger log = LoggerFactory.getLogger(AppConfig.class);
    private static AppConfig instance;
    private Properties props;

    /**
     * Instancia AppConfig carregando as propriedades do arquivo app-config.properties na raiz do classpath
     *
     * @throws IOException
     */
    private AppConfig() throws IOException {
        log.debug("Carregando as configuracoes do aplicativo");
        props = new Properties();
        props.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("app-config.properties"));
    }

    /**
     * Recupera a instância criada no método newInstance. Caso seja nula, lança uma exceção
     * ApplicationException.
     *
     * @return
     */
    public static AppConfig getInstance() {
        if (instance == null) {
            try {
                instance = new AppConfig();
            } catch (IOException e) {
                throw new Error("Erro ao carregar as propriedades do arquivo app-config.properties");
            }
        }
        return instance;
    }

    public String getCertAlias() {
        return props.getProperty("org.apache.ws.security.crypto.merlin.keystore.alias");
    }

    public String getCertPassword() {
        return props.getProperty("org.apache.ws.security.crypto.merlin.keystore.password");
    }

    public LocalDate getDate(String key, LocalDate defaultValue) {
        if (props.containsKey(key)) {
            String dateString = getString(key, null);
            return LocalDate.parse(dateString, DateTimeFormatter.ISO_LOCAL_DATE);
        } else {
            return defaultValue;
        }
    }

    public LocalDateTime getDateTime(String key, LocalDateTime defaultValue) {
        if (props.containsKey(key)) {
            String dateTimeString = getString(key, null);
            return LocalDateTime.parse(dateTimeString, DateTimeFormatter.ISO_LOCAL_DATE_TIME);
        } else {
            return defaultValue;
        }
    }

    /**
     * Retorna o inteiro configurado para a chave passada com o argumento key. Se chave não estiver
     * definida ou o valor não for um inteiro válido, retorna o argumento defaultValue.
     *
     * @param key Chave
     * @return
     */
    public Integer getInteger(String key, Integer defaultValue) {
        if (!props.containsKey(key)) {
            log.warn("Arquivo de configuração não possui chave={}. Retornado valor default={}", key, defaultValue);
            return defaultValue;
        }

        String val = props.getProperty(key);
        if (StringUtils.isBlank(val)) {
            log.warn("Chave={} configurada com brancos. Retornado valor default={}", key, defaultValue);
            return defaultValue;
        }
        if (!StringUtils.isNumeric(val)) {
            log.warn("Chave={} configurada com valor={}, que não é um número válido.", key, val);
            throw new ApplicationException("Arquivo de configuração com erro na chave " + key + ": o valor " + val +
                    " não é um número válido");
        }
        return Integer.valueOf(val);
    }

    public Properties getProperties() {
        return props;
    }

    public String getProperty(String propName) {
        return props.getProperty(propName);
    }

    /**
     * Retorna o valor da chave key. Se a chave não for encontrada, retorna o valor
     * defaultValue, salvando esse valor em arquivo se não for nulo.
     *
     * @param key
     * @param defaultValue
     * @return
     */
    public String getString(String key, String defaultValue) {
        if (props.containsKey(key)) {
            return props.getProperty(key);
        } else {
            return defaultValue;
        }
    }

    public LocalTime getTime(String key, LocalTime defaultValue) {
        if (props.containsKey(key)) {
            String timeString = getString(key, null);
            return LocalTime.parse(timeString, DateTimeFormatter.ISO_LOCAL_TIME);
        } else {
            return defaultValue;
        }
    }
}
