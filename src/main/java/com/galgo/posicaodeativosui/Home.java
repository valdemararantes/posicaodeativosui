package com.galgo.posicaodeativosui;

import com.galgo.cxfutils.ws.AmbienteEnum;
import com.galgo.posicaodeativos.PosicaoDeAtivos;
import com.galgo.posicaodeativosui.backend.ConsumirPosAtivosHelper;
import com.galgo.posicaodeativosui.backend.PosAtivosFileNameBuilder;
import com.galgo.utils.ApplicationException;
import com.galgo.utils.CodSTIParser;
import com.galgo.utils.cert_digital.TrustAllCerts;
import com.sistemagalgo.schemaposicaoativos.MessagePosicaoAtivosComplexType;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.geometry.Side;
import javafx.scene.Cursor;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.stage.DirectoryChooser;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXB;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.URL;
import java.text.MessageFormat;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

/**
 * Created by valdemar.arantes on 04/01/2016.
 */
public class Home {

    private static final Logger log = LoggerFactory.getLogger(Home.class);

    @FXML private ResourceBundle resources;

    @FXML private Label lblPreferences;

    @FXML private URL location;

    @FXML private BorderPane mainPane;

    @FXML private ChoiceBox choiceAmbiente;

    @FXML private TextField txtLogin;

    @FXML private PasswordField txtSenha;

    @FXML private TextField txtPasta;

/*
    @FXML private Label lblIdentMensagem;

    @FXML private TextField txtIdentMensagem;
*/

    @FXML private Label lblDtPosAtivos;

    @FXML private DatePicker dtpickerDtPosAtivos;

    @FXML private Label lblListaCodSTI;

    @FXML private TextField txtListaCodSTI;

    @FXML private Label lblMoeda;

    @FXML private TextField txtMoeda;

    @FXML private Button btnConsumir;

    @FXML private Label lblVersion;

    private Config config;
    /**
     * Campo auxiliar para ser atualizadoa pelo método consumir(codSTI).
     * Este campo deve ser alterado para false se pelo menos um retorno for não nulo.
     * Ele é utilizado para decidir qual mensagem deve ser apresentada após a conclusão
     * do consumo de uma lista de CodSTI.
     */
    private boolean allReturnEmpty = true;

    @FXML
    public void pnlFooterMouseReleased(Event event) {
        System.out.println("pnlFooterMouseReleased");
    }

    @FXML
    void VPNPreferences(ActionEvent event) {
        MainUI.getInstance().openVPNPreferences();
    }

    @FXML
    void configFolderPreferences(ActionEvent event) {
        MainUI.getInstance().openConfigPreferences();
    }

    /**
     * Método que consulta a posição de ativos do fundo e salva na pasta definida no campo folder
     *
     * @param codSTI Código STI do fundo a ser consultado
     * @return Indica se o Galgo retornou a posição (true) ou não (false)
     */
    private boolean consumir(Integer codSTI) {
        log.debug("Iniciando método consumir(codSTI={})", codSTI);
        PosicaoDeAtivos posAtivos = new PosicaoDeAtivos();
        try {
            String ambStr = (String) choiceAmbiente.getSelectionModel().getSelectedItem();
            if ("Homologação".equals(ambStr)) {
                posAtivos.setAmbiente(AmbienteEnum.HOMOLOGACAO);
            } else {
                posAtivos.setAmbiente(AmbienteEnum.PRODUCAO);
            }
            posAtivos.setUser(txtLogin.getText(), txtSenha.getText());
            String folder = StringUtils.trim(txtPasta.getText());
            posAtivos.setIdMsgSender(AppConfig.getInstance().getProperty("id_msg_sender"));
            posAtivos.setDt(dtpickerDtPosAtivos.getValue());
            posAtivos.setMoeda(txtMoeda.getText());

            posAtivos.addCdObjetos(codSTI, null, null, null);

            log.info("Consumindo todas as páginas...");
            final MessagePosicaoAtivosComplexType resp = new ConsumirPosAtivosHelper(posAtivos)
                .consumirTodasAsPaginas();
            log.info("Consumo concluído.");
            if (resp == null) {
                log.info("Retorno para codSTI={} NULO", codSTI);
                return false;
            } else {
                log.info("Retorno para codSTI={} NÃO NULO", codSTI);

                // Atualizando o campo auxiliar.
                allReturnEmpty = false;

                String filename = folder + "/" + PosAtivosFileNameBuilder.build(codSTI, dtpickerDtPosAtivos.getValue());
                log.info("Salvando as informações no arquivo {}", filename);
                Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filename), "UTF-8"));
                JAXB.marshal(resp, out);
                out.close();
                log.info("Informações salvas");
                return true;
            }
        } catch (ApplicationException e) {
            throw e;
        } catch (Throwable e) {
            log.error(null, e);
            throw new ApplicationException("Ocorreu um erro no aplicativo: " + e.toString());
        }
    }

    private void fillFielsForTest() {
        if (isTest()) {
            txtLogin.setText(System.getProperty("posativos.login"));
            txtSenha.setText(System.getProperty("posativos.pwd"));
            //txtIdentMensagem.setText(System.getProperty("posativos.identif.msg"));
            txtListaCodSTI.setText(System.getProperty("posativos.cod.sti"));
            dtpickerDtPosAtivos.getEditor().setText(System.getProperty("posativos.data"));
        }
    }

    /**
     * Método para selecionar a pasta de desitno dos arquivos
     *
     * @param event
     */
    @FXML
    void handleBtnPastaAction(ActionEvent event) {
        log.info("Botão Selecionar Pasta acionado");
        DirectoryChooser chooser = new DirectoryChooser();
        chooser.setTitle(AppConfig.getInstance().getProperty("msg.title.select_folder"));
        File defaultDirectory = new File(".");
        chooser.setInitialDirectory(defaultDirectory);
        File selectedDirectory = chooser.showDialog(null);
        if (selectedDirectory != null) {
            log.info("Pasta selecionada: {}", selectedDirectory.getAbsolutePath());
            txtPasta.setText(selectedDirectory.getAbsolutePath());
        } else {
            log.info("Nenhuma pasta selecionada");
        }
    }

    /**
     * Método executado quando o botão "Consumir" é acionado
     *
     * @param event
     */
    @FXML
    void handleButtonAction(ActionEvent event) {
        Task<Void> task = new Task<Void>() {
            protected Void call() throws Exception {
                Platform.runLater(() -> mainPane.getScene().setCursor(Cursor.WAIT));
                VpnHandler vpnHandler = new VpnHandler();
                try {
                    log.info("Botão Consumir clicado");

                    log.info("Validando os campos do formulário");
                    if (!validate()) {
                        return null;
                    }

                    // Controlando (ou não) a VPN do usuário
                    vpnHandler.setControlled(config.getBoolean("app.vpn.controlled", false)).setName(config.getProperty(
                        "app.vpn.name")).setLogin(config.getProperty("app.vpn.user")).setPwd(CryptoUtils.decrypt(
                        config.getProperty("app.vpn.pwd")));
                    try {
                        vpnHandler.turnOn();
                    } catch (ApplicationException e) {
                        Platform.runLater(() -> com.galgo.posicaodeativosui.Alert.show(e.getMessage(),
                            Alert.AlertType.INFORMATION));
                        return null;
                    }

                    // Salvnaod os dados do formulário em um arquivo de configuração para ser reusado
                    // em uma outra execução
                    saveFormValues();

                    final List<Integer> codSTIList = CodSTIParser.parse(txtListaCodSTI.getText());
                    final LocalDate dt = dtpickerDtPosAtivos.getConverter().fromString(
                        dtpickerDtPosAtivos.getEditor().getText());
                    dtpickerDtPosAtivos.setValue(dt);

                    allReturnEmpty = true;
                    int qtdFundosPossuemPosicao = 0;
                    for (Integer codSTI : codSTIList) {
                        try {
                            boolean fundoPossuiPosicao = consumir(codSTI);
                            if (fundoPossuiPosicao) {
                                qtdFundosPossuemPosicao++;
                            }
                        } catch (Exception e) {
                            if (!showErrorAndContinue(e.getMessage())) {
                                log.info("Botão Encerrar Requisições acionado.");
                                return null;
                            }
                        }
                    }
                    if (allReturnEmpty) {
                        showInfo(AppConfig.getInstance().getProperty("msg.empty_return"));
                    } else {
                        showInfo(MessageFormat.format(AppConfig.getInstance().getProperty("msg.info_saved"),
                            qtdFundosPossuemPosicao, codSTIList.size()));
                    }
                } finally {
                    // Desligando (ou não) a VPN do usuário
                    vpnHandler.turnOff();

                    Platform.runLater(() -> mainPane.getScene().setCursor(Cursor.DEFAULT));
                }

                return null;
            }
        };

        Thread th = new Thread(task);
        th.setDaemon(true);
        th.start();
    }

    @FXML
    void initialize() {
        assert mainPane != null : "fx:id=\"mainPane\" was not injected: check your FXML file 'Home.fxml'.";
        assert choiceAmbiente != null;
/*
        assert lblIdentMensagem != null :
            "fx:id=\"lblIdentMensagem\" was not injected: check your FXML file 'Home" + ".fxml'.";
        assert txtIdentMensagem != null :
            "fx:id=\"txtIdentMensagem\" was not injected: check your FXML file 'Home" + ".fxml'.";
*/
        assert lblDtPosAtivos != null : "fx:id=\"lblDtPosAtivos\" was not injected: check your FXML file 'Home.fxml'.";
        assert dtpickerDtPosAtivos != null :
            "fx:id=\"dtpickerDtPosAtivos\" was not injected: check your FXML file " + "'Home.fxml'.";
        assert lblListaCodSTI != null : "fx:id=\"lblListaCodSTI\" was not injected: check your FXML file 'Home.fxml'.";
        assert txtListaCodSTI != null : "fx:id=\"txtListaCodSTI\" was not injected: check your FXML file 'Home.fxml'.";
        assert lblMoeda != null : "fx:id=\"lblMoeda\" was not injected: check your FXML file 'Home.fxml'.";
        assert txtMoeda != null : "fx:id=\"txtMoeda\" was not injected: check your FXML file 'Home.fxml'.";
        assert btnConsumir != null : "fx:id=\"btnConsumir\" was not injected: check your FXML file 'Home.fxml'.";
        assert lblVersion != null : "fx:id=\"lblVersion\" was not injected: check your FXML file 'Home.fxml'.";

        lblPreferences.setOnMouseClicked(ev -> {
            lblPreferences.getContextMenu().show(lblPreferences, Side.LEFT, 0, 0);
        });

        initializeFormValues();

        TrustAllCerts.doIt();
    }

    /**
     * Popula os campos do formulário com seus valores iniciais. Os seguintes campo são lidos do arquivo
     * de configuração .posicao_de_ativos\config.properties:
     * <br/>
     * <ul>
     * <li>ambiente</li>
     * <li>user.login</li>
     * <li>user.password</li>
     * <li>folder</li>
     * </ul>
     * <p>
     * A versão do app é lida do arquivo app-config.properties na raiz do classpath.
     */
    private void initializeFormValues() {
        try {
            config = Config.getInstance();
            choiceAmbiente.setValue(config.getString(Config.AMBIENTE_KEY, "Produção"));
            txtLogin.setText(config.getUsername());
            txtSenha.setText(CryptoUtils.decrypt(config.getPassword()));
            txtPasta.setText(config.getString(Config.FOLDER_KEY, new File(".").getCanonicalPath()));

            lblVersion.setText(AppConfig.getInstance().getString("app.version", "Sem versão"));

            fillFielsForTest();
        } catch (Exception e) {
            log.error(null, e);
        }
    }

    /**
     * @return true se a variável de ambiente "posativos.teste" for definida como true (ignora maiúsculas/minúsculas)
     */
    private boolean isTest() {
        return "true".equalsIgnoreCase(System.getProperty("posativos.teste"));
    }

    /**
     * Se não for uma execução de teste, salva no arquivo de configuração do usuário os campos
     * login, senha, ambiente e pasta.
     */
    private void saveFormValues() {

        if (isTest()) {
            log.warn("Execução de teste. Valores não serão salvos no arquivo de configuração.");
            return;
        }

        log.info("Salvando alguns dos campos no arquivo de configuração do usuário");

        //@formatter:off
        Config.getInstance()
            .setPassword(CryptoUtils.encrypt(txtSenha.getText()))
            .setUsername(txtLogin.getText())
            .addProperty(Config.AMBIENTE_KEY, choiceAmbiente.getValue())
            .addProperty(Config.FOLDER_KEY, txtPasta.getText());
        //@formatter:on
    }

    /**
     * @param txt
     * @return Booleano que indica se as requisições devem prosseguir (true) ou não
     */
    private boolean showErrorAndContinue(String txt) {
        Boolean ret[] = new Boolean[]{null};
        Platform.runLater(() -> {
            Alert alert = new Alert(Alert.AlertType.ERROR, txt);
            ButtonType btnOk = new ButtonType("OK");
            ButtonType btnCancelRequest = new ButtonType(AppConfig.getInstance().getProperty(
                "btn.caption.cancel_request"));
            alert.getButtonTypes().setAll(btnOk, btnCancelRequest);
            final Optional<ButtonType> result = alert.showAndWait();
            log.info("Alert fechado");
            ret[0] = result.get() == btnOk;
        });
        while (true) {
            if (ret[0] == null) {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException ignored) {
                }
            } else {
                return ret[0];
            }
        }
    }

    private void showInfo(String txt) {
        Platform.runLater(() -> com.galgo.posicaodeativosui.Alert.show(txt, Alert.AlertType.INFORMATION));
    }

    /**
     * @return True se tudo OK
     */
    private boolean validate() {
        final String ambStr = (String) choiceAmbiente.getSelectionModel().getSelectedItem();
        log.info("ambiente: {}", ambStr);
        if (ambStr == null) {
            showInfo("O ambiente de execução é obrigatório");
            return false;
        }

        log.info("login: {}", txtLogin.getText());
        if (StringUtils.isBlank(txtLogin.getText())) {
            showInfo("O login do usuário externo é obrigatório");
            return false;
        }

        if (StringUtils.isBlank(txtSenha.getText())) {
            showInfo("A senha do usuário externo é obrigatória");
            return false;
        }

        log.info("pasta: {}", txtPasta.getText());
        if (StringUtils.isBlank(txtPasta.getText())) {
            showInfo("Defina uma pasta para salvar os arquivos");
            return false;
        }

/*
        log.info("ident. da mensagem: {}", txtIdentMensagem.getText());
        if (StringUtils.isBlank(txtIdentMensagem.getText())) {
            showInfo("O identificador da mensagem é obrigatório");
            return false;
        }
*/

        log.info("lista de codSTI: {}", txtListaCodSTI.getText());
        if (StringUtils.isBlank(txtListaCodSTI.getText())) {
            showInfo("A lista de códigos STI deve possuir ao menos um código STI");
            return false;
        } else {
            try {
                List<Integer> codSTIList = CodSTIParser.parse(txtListaCodSTI.getText());
                log.debug("Lista de códigos STI com {} códigosI", codSTIList.size());
                if (codSTIList.isEmpty()) {
                    showInfo("A lista de códigos STI deve possuir ao menos um código STI");
                    return false;
                }
            } catch (ParseException e) {
                showInfo("Erro ao ler os códigos STI [elemento " + e.getErrorOffset() + "]");
                return false;
            }
        }

        final LocalDate dt = dtpickerDtPosAtivos.getValue();
        final String dtStr = dtpickerDtPosAtivos.getEditor().getText();
        log.info("dt={}; dtStr={}", dt, dtStr);
        if (dt == null && StringUtils.isBlank(dtpickerDtPosAtivos.getEditor().getText())) {
            showInfo("A data é obrigatória");
            return false;
        } else {
            try {
                dtpickerDtPosAtivos.getConverter().fromString(dtStr);
            } catch (Exception e) {
                String errMsg = String.format("Erro ao converter %s para data: %s", dtStr, e.toString());
                log.info(errMsg);
                showInfo(errMsg);
                return false;
            }
        }

        log.info("moeda: {}", txtMoeda.getText());
        if (StringUtils.isBlank(txtMoeda.getText())) {
            showInfo("A moeda é obrigatória");
            return false;
        }

        return true;
    }
}
