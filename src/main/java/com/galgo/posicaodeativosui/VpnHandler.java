package com.galgo.posicaodeativosui;

import com.galgo.utils.ApplicationException;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.MessageFormat;

/**
 * Created by valdemar.arantes on 13/04/2016.
 */
class VpnHandler {

    private static final Logger log = LoggerFactory.getLogger(VpnHandler.class);
    private static final AppConfig appConf = AppConfig.getInstance();
    private boolean controlled;
    private String login;
    private String pwd;
    private String name;
    private boolean prevConnected = false;
    private Rasdial rasdial;

    public String getLogin() {
        return login;
    }

    public VpnHandler setLogin(String login) {
        this.login = login;
        return this;
    }

    public String getName() {
        return name;
    }

    public VpnHandler setName(String name) {
        this.name = name;
        return this;
    }

    public String getPwd() {
        return pwd;
    }

    public VpnHandler setPwd(String pwd) {
        this.pwd = pwd;
        return this;
    }

    public boolean isControlled() {
        return controlled;
    }

    public VpnHandler setControlled(boolean controlled) {
        this.controlled = controlled;
        return this;
    }

    /**
     * Se a VPN estava desligada antes de ligá-la no passo anterior, desligo aqui.
     */
    public void turnOff() {
        try {
            if (!prevConnected && rasdial != null && rasdial.isConnected()) {
                rasdial.disconnect();
                log.info("VPN desconectada");
            }
        } catch (Exception e) {
            throw new ApplicationException("Erro ao desconectar a VPN", e);
        }
    }

    /**
     * Liga a VPN se o nome, usuário e senha estiverem definidose e se a VPN estava desligada.
     *
     * @return
     */
    public void turnOn() {
        if (!controlled) {
            log.info(appConf.getProperty("msg.vpn.not_controoled"));
            return;
        }

        log.info(appConf.getProperty("msg.vpn.controlled"));
        validateFields();

        log.info("VPN configurada com nome {}", name);
        this.rasdial = new Rasdial(name);
        try {
            prevConnected = this.rasdial.isConnected();
            log.info("VPN previamente conectada? {}", prevConnected);
            if (!prevConnected) {
                boolean connected = this.rasdial.connect(login, pwd);
                if (!connected) {
                    String errMsg = MessageFormat.format(appConf.getProperty("err.vpn.connection"), name);
                    log.warn(errMsg);
                    throw new ApplicationException(errMsg);
                } else {
                    log.info("VPN conectada com sucesso");
                }
            }
        } catch (ApplicationException e) {
            throw e;
        } catch (Exception e) {
            throw new ApplicationException("Erro ao conectar a VPN", e);
        }
    }

    private void validateFields() {
        log.info("Validando os parâmetros da VPN");
        if (StringUtils.isBlank(name)) {
            throw new ApplicationException("O nome da VPN não está preenchido");
        }
        if (StringUtils.isBlank(login)) {
            throw new ApplicationException("O usuário da VPN não está preenchido");
        }
        if (StringUtils.isBlank(pwd)) {
            throw new ApplicationException("A senha da VPN não está preenchida");
        }
    }
}
