package com.galgo.posicaodeativosui;

import com.galgo.utils.ApplicationException;
import com.galgo.utils.Config;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.input.KeyCode;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;

/**
 * Created by valdemar.arantes on 04/01/2016.
 */
public class MainUI extends Application {

    private static final Logger log = LoggerFactory.getLogger(MainUI.class);

    public static final String CONFIG_FILE_PATH = "config-file.path";
    public static final int ERR_CODE_APP_PROPERTIES_NOT_FOUND = -10;

    public static com.galgo.utils.Config externalConfig;
    private static MainUI instance;
    public Stage primaryStage;

    public static MainUI getInstance() {
        return instance;
    }

    public static void main(String[] args) {
        log.info("Iniciando o aplicativo: {}", AppConfig.getInstance().getString("app.version", "Sem versão"));
        try {
            loadExternalConfigFile();
        } catch (URISyntaxException e) {
            log.error(null, e);
            System.exit(-100);
        }
        launch(args);
    }

    /**
     * Carrega o arquivo externo de configurações do aplicativo utilizando as seguintes regras:
     * <ul>
     *
     * <li>Se a variável de ambiente <code>config-file.path</code> estiver definida, utiliza seu valor como caminho
     * do arquivo;</li>
     *
     * <li>Caso contráveio, utiliza o arquivo app.properties encontrado na raiz do classpath</li>
     *
     * </ul>
     * <p>Caso o arquivo não seja encontrado, encerra o aplicativo com código -10.</p>
     */
    private static void loadExternalConfigFile() throws URISyntaxException {
        File externalConfigFile;

        // Arquivo definido na variável de ambiente config-file.path
        final String externalConfigFilePathStr = System.getProperty(CONFIG_FILE_PATH);
        log.info("externalConfigFilePathStr={}", externalConfigFilePathStr);
        if (StringUtils.isNotBlank(externalConfigFilePathStr)) {
            log.info("Carregando arquivo externo de configurações do app definido na variável de ambiente: {}", externalConfigFilePathStr);
            externalConfigFile = new File(externalConfigFilePathStr);
        } else {
            final URL appUrl = Thread.currentThread().getContextClassLoader().getResource("app.properties");
            log.info("Carregando arquivo externo de configurações do app a partir do classpath: {}", appUrl.toString());
            externalConfigFile = new File(appUrl.toURI());
        }

        if (!externalConfigFile.isFile()) {
            log.error("O arquivo de configuração {} não foi encontrado. Encerrando o aplicativo...",
                externalConfigFile.getAbsolutePath());
            System.exit(ERR_CODE_APP_PROPERTIES_NOT_FOUND);
        }

        externalConfig = new Config(externalConfigFile);
    }

    /**
     * The application initialization method. This method is called immediately
     * after the Application class is loaded and constructed. An application may
     * override this method to perform initialization prior to the actual starting
     * of the application.
     * <p>
     * <p>
     * The implementation of this method provided by the Application class does nothing.
     * </p>
     * <p>
     * <p>
     * NOTE: This method is not called on the JavaFX Application Thread. An
     * application must not construct a Scene or a Stage in this
     * method.
     * An application may construct other JavaFX objects in this method.
     * </p>
     */
    @Override
    public void init() throws Exception {
        super.init();
    }

    public void openConfigPreferences() {
        log.info("Início");
        openPreferencesDialog("app.window.prefs_config.folder", ConfigFolderPreferences.class,
            "ConfigFolderPreferences.fxml", "Erro na tela de preferências de configuração");
    }

    public void openVPNPreferences() {
        log.info("Início");
        openPreferencesDialog("app.window.prefs_vpn.title", VpnPreferences.class, "VpnPreferences.fxml",
            "Erro na tela de configuração da VPN");
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;
        instance = this;
        Parent root = FXMLLoader.load(getClass().getResource("Home.fxml"));
        primaryStage.setTitle(AppConfig.getInstance().getProperty("app.window.home"));
        primaryStage.setScene(new Scene(root, 620, 400));
        primaryStage.setResizable(false);
        primaryStage.show();
    }

    private void openPreferencesDialog(String titleKey, Class<?> resourceClass, String fxmlFileName, String errMsg) {
        try {
            AppConfig appConf = AppConfig.getInstance();

            Stage stage = new Stage();
            stage.setTitle(appConf.getProperty(titleKey));
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.initOwner(primaryStage);
            Parent root = FXMLLoader.load(resourceClass.getResource(fxmlFileName));
            Scene scene = new Scene(root);
            scene.setOnKeyPressed(ev -> {
                if (KeyCode.ESCAPE.equals(ev.getCode())) {
                    log.debug("Tecla ESC pressionada. Fechando a janela de preferências...");
                    stage.close();
                }
            });
            stage.setScene(scene);
            stage.setResizable(false);
            log.debug("Aguardando a Tela de prederências...");
            stage.showAndWait();
            log.debug("Tela de preferências fechada.");
        } catch (ApplicationException e) {
            log.error(null, e);
            com.galgo.posicaodeativosui.Alert.show(e.getMessage(), Alert.AlertType.ERROR);
        } catch (Exception e) {
            log.error(null, e);
            com.galgo.posicaodeativosui.Alert.show(errMsg, Alert.AlertType.ERROR);
        }
    }
}