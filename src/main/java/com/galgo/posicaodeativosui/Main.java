package com.galgo.posicaodeativosui;

import com.galgo.cxfutils.ws.AmbienteEnum;
import com.galgo.posicaodeativos.PosicaoDeAtivos;
import com.galgo.posicaodeativosui.backend.PosAtivosFileNameBuilder;
import com.galgo.utils.ApplicationException;
import com.galgo.utils.CodSTIParser;
import com.galgo.utils.cert_digital.TrustAllCerts;
import com.google.common.collect.Lists;
import com.sistemagalgo.schemaposicaoativos.MessagePosicaoAtivosComplexType;
import com.sistemagalgo.serviceposicaoativos.ConsumirFaultMsg;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXB;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.text.MessageFormat;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Properties;

/**
 * Created by Valdemar.Arantes on 29/01/2016.
 */
public class Main {

    private static final Logger log = LoggerFactory.getLogger(Main.class);
    private static final String errFmt = "Defina a variavel de ambiente %s\n";

    private static final String ENV_LOGIN = "posativos.login";
    private static final String ENV_PWD = "posativos.pwd";
    private static final String ENV_AMB = "posativos.amb";
    private static final String ENV_FILE = "posativos.arquivo";
    private static final String ENV_ID_MSG_SENDER = "posativos.identif.msg";
    private static final String ENV_COD_STI = "posativos.cod.sti";
    private static final String ENV_DT = "posativos.data";
    private static final String ENV_MOEDA = "posativos.moeda";
    private static final String ENV_RPT_MARKER = "posativos.rpt.marker";
    private static final String ENV_ASS_MARKER = "posativos.ass.marker";

    // VPN
    private static final String ENV_VPN_NAME = "posativos.vpn.name";
    private static final String ENV_VPN_LOGIN = "posativos.vpn.login";
    private static final String ENV_VPN_PWD = "posativos.vpn.pwd";

    private String amb;
    private String login;
    private String pwd;
    private File folder;
    private String idMsgSender;
    private List<Integer> codSTIList;
    private LocalDate dt;
    private String moeda;
    private String rptMarker;
    private Long assMarker;
    private static final AppConfig appConf = AppConfig.getInstance();

    public static void main(String[] args) {
        log.info("Iniciando o aplicativo. Versão: {}", appConf.getProperty("app.version"));
        int returnCode = 0;
        try {
            TrustAllCerts.doIt();

            StringBuilder err = validateEnvRequired();
            if (StringUtils.isNotBlank(err.toString())) {
                System.out.printf(err.toString());
                return;
            }

            Main m = buildMain(err);
            if (StringUtils.isNotBlank(err.toString())) {
                System.out.printf(err.toString());
                return;
            }

            final int qtdFundosPossuemPosicao = m.consumir();
            log.info(MessageFormat.format(appConf.getProperty("msg.info_saved"),
                qtdFundosPossuemPosicao, m.codSTIList.size()));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            returnCode = -1;
        } finally {
            log.info("Fim da execução.");
        }

        System.exit(returnCode);
    }

    private static Main buildMain(StringBuilder err) {
        log.debug("Parse dos parâmetros de entrada");
        Main m = new Main();
        m.amb = getEnv(ENV_AMB);
        if (!"H".equalsIgnoreCase(m.amb) && !"P".equalsIgnoreCase(m.amb)) {
            err.append(ENV_AMB + " deve ser definido com o valor H ou P\n");
        }

        m.login = getEnv(ENV_LOGIN);

        m.pwd = getEnv(ENV_PWD);

        m.folder = new File(getEnv(ENV_FILE));
        if (!m.folder.isDirectory()) {
            err.append("Pasta " + m.folder + " nao encontrada.\n");
        }

        // Este argumento não será mais utiliado. A mensagem é a definida no arquivo app-config.properties
        //m.idMsgSender = getEnv(ENV_ID_MSG_SENDER);
        m.idMsgSender = appConf.getProperty("id_msg_sender");

        try {
            m.codSTIList = CodSTIParser.parse(getEnv(ENV_COD_STI));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        try {
            m.dt = LocalDate.parse(getEnv(ENV_DT), DateTimeFormatter.ofPattern("dd/MM/yyyy"));
        } catch (Exception e) {
            String msg = MessageFormat.format(appConf.getProperty("err.data.invalida"), getEnv(ENV_DT));
            log.info(msg);
            err.append(msg).append("\n");
        }

        m.moeda = getEnv(ENV_MOEDA);

        m.rptMarker = getEnv(ENV_RPT_MARKER);

        if (StringUtils.isNotBlank(getEnv(ENV_ASS_MARKER))) {
            try {
                m.assMarker = Long.valueOf(getEnv(ENV_ASS_MARKER));
            } catch (NumberFormatException e) {
                String msg = String.format("%s nao eh um numero valido", getEnv(ENV_ASS_MARKER));
                log.info(msg);
                err.append(msg).append("\n");
            }
        }

        return m;
    }

    private static String getEnv(String envAmb) {
        return System.getProperty(envAmb);
    }

    private static StringBuilder validateEnvRequired() {
        log.debug("Validando...");
        final StringBuilder err = new StringBuilder();
        final Properties sisProps = System.getProperties();
        List<String> envs = Lists.newArrayList(ENV_AMB, ENV_LOGIN, ENV_PWD, ENV_FILE, ENV_ID_MSG_SENDER, ENV_COD_STI,
            ENV_DT, ENV_MOEDA);
        envs.forEach(env -> {
            log.debug("{}=[{}]", env, ENV_PWD.equals(env) ? "******" : getEnv(env));
            if (StringUtils.isBlank(getEnv(env))) {
                err.append(String.format(errFmt, env));
            }
        });
        return err;
    }

    /**
     * Consome as informações para cada fundo, salvando em arquivos próprios.
     *
     * @return Qtd de fundos que têm posição de ativos.
     */
    private int consumir() {
        log.debug("Início...");


        boolean allReturnEmpty = true;
        int qtdFundosPossuemPosicao = 0;

        VpnHandler vpnHandler = initVpn();
        try {
            vpnHandler.turnOn();


            for (Integer codSTI : this.codSTIList) {
                log.debug("Populando posAtivos: codSTI={}", codSTI);
                PosicaoDeAtivos posAtivos = new PosicaoDeAtivos();
                try {


                    if ("H".equalsIgnoreCase(amb)) {
                        posAtivos.setAmbiente(AmbienteEnum.HOMOLOGACAO);
                    } else {
                        posAtivos.setAmbiente(AmbienteEnum.PRODUCAO);
                    }
                    posAtivos.setUser(login, pwd);
                    posAtivos.setIdMsgSender(idMsgSender);
                    posAtivos.setDt(dt);
                    posAtivos.setMoeda(moeda);

                    if (assMarker != null || rptMarker != null) {
                        posAtivos.setMarkers(assMarker, rptMarker);
                    }


                    posAtivos.addCdObjetos(codSTI, null, null, null);

                    log.info("Consumindo as informações...");
                    final MessagePosicaoAtivosComplexType resp = posAtivos.consumir();
                    if (resp != null) {
                        qtdFundosPossuemPosicao++;
                    }

                    File posAtivosFile = new File(folder, PosAtivosFileNameBuilder.build(codSTI, dt));
                    log.info("Consumo concluído com sucesso. Salvando as informações no arquivo {}",
                        posAtivosFile.getCanonicalPath());
                    Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(posAtivosFile),
                        "UTF-8"));
                    JAXB.marshal(resp, out);
                    out.close();
                    log.info("Informações salvas em arquivo");
                } catch (ConsumirFaultMsg e) {
                    log.error("\n\n{}\n\n", posAtivos.getRequestResponseString());
                } catch (Exception e) {
                    log.error(null, e);
                }
            }
            return qtdFundosPossuemPosicao;
        } finally {
            vpnHandler.turnOff();
        }
    }

    /**
     * Inicializa a classe VpnHandler, mas não conecta
     *
     * @return
     */
    private VpnHandler initVpn() {
        if (StringUtils.isNotBlank(getEnv(ENV_VPN_NAME))) {
            if (StringUtils.isBlank(getEnv(ENV_VPN_LOGIN))) {
                throw new ApplicationException(appConf.getProperty("err.vpn.login.required"));
            }
            if (StringUtils.isBlank(getEnv(ENV_VPN_PWD))) {
                throw new ApplicationException(appConf.getProperty("err.vpn.pwd.required"));
            }
        }
        VpnHandler vpnHandler = new VpnHandler();
        vpnHandler.setControlled(StringUtils.isNotBlank(getEnv(ENV_VPN_NAME))).setLogin(getEnv(ENV_VPN_LOGIN)).setName(
            getEnv(ENV_VPN_NAME)).setPwd(getEnv(ENV_VPN_PWD));
        return vpnHandler;
    }
}
