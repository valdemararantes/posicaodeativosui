package com.galgo.posicaodeativosui;

import javafx.scene.control.ButtonType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by valdemar.arantes on 18/05/2016.
 */
public class Alert {

    private static final Logger log = LoggerFactory.getLogger(Alert.class);

    public static void show(String txt, javafx.scene.control.Alert.AlertType type) {
        javafx.scene.control.Alert alert = new javafx.scene.control.Alert(type, txt);
        alert.showAndWait().filter(response ->
            response == ButtonType.OK).ifPresent(response ->
                log.info("Alert fechado"));
    }
}
